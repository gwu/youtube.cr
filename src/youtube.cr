require "json"
require "xml"
require "http"
require "html"
require "uri"

module YouTube
  extend self

  def to_ytid(vid)
    return vid if vid.size == 11

    uri = URI.parse(vid)

    host = uri.host
    return nil unless host
    path = uri.path
    return nil unless path
    return path.lchop('/') if host.includes? "youtu.be"

    query = uri.query
    return nil unless query

    vid = query.split("?").find(&.starts_with?("v="))
    return nil unless vid
    return vid.split("=")[1]
  end

  def search(query : String)
    return nil if query.empty?
    uri = URI.parse("https://www.youtube.com/results")
    uri.query = "search_query=" + URI.escape(query)
    XML.parse_html(HTTP::Client.get(uri).body.not_nil!)
      .xpath_nodes("//div[@class='yt-lockup-content']")
  end
end
