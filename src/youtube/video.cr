module YouTube
  class Video
    property id
    property title
    property channel_name
    property channel
    property formats

    @@endpoint = "https://www.youtube.com"

    def initialize(@id : String, @title : String,
                   @channel : String, @channel_name : String,
                   @formats : Hash(Int32, YouTube::Format))
    end

    def self.get_by_id(id : String)
      response = HTTP::Client.get(URI.parse("#{@@endpoint}/watch?v=#{id}")).body
      return nil unless response

      script = XML.parse_html(response)
        .xpath_nodes("//div[@id='player-mole-container']/script")
        .find(&.content.includes? "ytplayer.config =")
        .try(&.content)
      return nil unless script

      config = JSON.parse(
        script
          .match(/(?<=ytplayer\.config =).*?(?=;ytplayer\.load = function())/)
          .not_nil![0]
      )
      return nil unless config

      formats = Hash(Int32, YouTube::Format).new

      data = JSON.parse(config["args"]["player_response"].to_s)
      data.["streamingData"].as_h.select({"formats", "adaptiveFormats"})
        .values.each do |_f|
          _f.as_a.each do |f|
            mime = f["mimeType"].to_s.split("; codecs=")
            codecs = mime[1].strip('"').split(", ")

            formats = formats.merge({f["itag"].to_s.to_i => YouTube::Format
                                       .new(f["itag"].to_s.to_i,
                                       f["url"].to_s,
                                       f["contentLength"].to_s.to_i64,
                                       mime.first,
                                       codecs,
                                       f["qualityLabel"]?.to_s,
                                       f["audioSampleRate"]?.to_s,
                                       f["audioQuality"]?.to_s.split("_").last.downcase
                                     )}
            )
          end
        end

      self.new(id, data["videoDetails"]["title"].to_s,
        data["videoDetails"]["channelId"].to_s,
        data["videoDetails"]["author"].to_s,
        formats)
    end

    def dl(format : Format, loc : String)
      uri = URI.parse(format.url)
      return unless uri

        file = File.new(loc.not_nil!, "w")
        dl(uri, file, format.content_length)
    end

    private def dl(uri, file, lim)
      client = HTTP::Client.new(uri)

      # See if this is an actual issue
      # "Error reading socket: Connection reset by peer"
      begin
        client.get(uri.full_path) do |stream|
          copy(stream.body_io, file, lim)
        end
      rescue ex
        file.close
        puts ex
      end
    end

    private def copy(src, dst, lim)
      buffer = uninitialized UInt8[4096]
      count = 0
      while (len = src.read(buffer.to_slice).to_i32) > 0 && lim > count
        begin
          dst.write buffer.to_slice[0, len]
        rescue
        end
        count += len
      end
      len < 0 ? len : count
    end
  end

  struct Format
    property tag
    property url
    property content_length
    property mime_type
    property codecs
    property resolution
    property audio_sample_rate
    property audio_quality

    def initialize(@tag : Int32, @url : String, @content_length : Int64,
                   @mime_type : String, @codecs : Array(String),
                   @resolution : String, @audio_sample_rate : String,
                   @audio_quality : String)
    end
  end
